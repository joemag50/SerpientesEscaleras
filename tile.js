
class Tile
{
	constructor(x, y, wh, index, nxt)
	{
		this.x = x;
		this.y = y;
		this.wh = wh;
		this.next = nxt;
		this.index = index;
		if (this.index % 2 == 1)
		{
			this.color = 225;
		}
		else
		{
			this.color = 15;
		}
	}

	show()
	{
		fill(this.color);
		rect(this.x, this.y, this.wh, this.wh);
		//fill(255);
		//textSize(20);
		//text(this.index + "->" + this.next, this.x, this.y + this.wh);
	}

	getCenter()
	{
		let cx = this.x + this.wh / 2;
		let cy = this.y + this.wh / 2;
		return [cx, cy];
	}
}