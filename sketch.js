
//JCGE Serpientes y Escaleras
let player;
let tiles = [];

let rolls = [];
let index = 0;
let averageRolls = 0;

let avgP;
// put setup code here
function setup()
{
	createCanvas(700,700);
	background(175,255,212);

	avgP = createP('');

	rolls[index] = 0;

	let resolution = 50;
	let cols = width / resolution;
	let rows = height / resolution;

	let x = 0;
	let y = (rows - 1) * resolution;
	let dir = 1;

	for (var i = 0; i < cols*rows; i++)
	{
		let tile = new Tile(x, y, resolution, i, i+1);
		tiles.push(tile);
		x = x + (resolution * dir);
		if (x >= width || x <= -resolution)
		{
			dir *= -1;
			x += resolution * dir;
			y -= resolution;
		}
	}

	player = new Player();
}

// put drawing code here
function draw()
{
	for (let tile of tiles)
	{
		tile.show();
	}

	player.roll();
	rolls[index]++;

	let gameOver = false;
	if (player.spot >= tiles.length-1)
	{
		player.spot = tiles.length-1;
		console.log("Game Over");
		gameOver = true;
	}
	
	player.show(tiles);

	if (gameOver)
	{
		player.reset();
		index++;
		rolls[index] = 0;
	}

	let sum = 0;
	for (var i = 0; i < rolls.length-1; i++) {
		sum += rolls[i];
	}

	let avg = sum / (rolls.length-1);
	avgP.html(avg);
}
